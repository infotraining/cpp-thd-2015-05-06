#include <iostream>
#include <thread>
#include <string>
#include <vector>

using namespace std;

void hello()
{
    cout << "Hello world" << endl;
}

void say_id(int id, string msg)
{
    cout << msg << " " << id << endl;
}

void increase(int& num)
{
    ++num;
}

struct F
{
    void operator ()(int id)
    {
        cout << "functor " << id << endl;
    }
};

class Buff
{
    vector<int> buff_;
public:
    void push(const vector<int>& item)
    {
        buff_.assign(item.begin(), item.end());
    }

    void print()
    {
        cout << "in buffer: ";
        for (const auto& el : buff_)
            cout << el << ", ";
        cout << endl;
    }
};

int main()
{
    thread th1(&hello);
    thread th2(&say_id, 10, "message");
    int a = 10;
    thread th3(&increase, ref(a));
    F f;
    thread th4(f, 10);
    thread th5([] {cout << "Lambda in thread" << endl;});
    Buff b;
    vector<int> v{1,2,3,4,5};
    thread th6(&Buff::push, &b, cref(v));
    th1.join();
    th2.join();
    th3.join();
    th4.join();
    th5.join();
    th6.join();
    b.print();
    cout << "a = " << a << endl;
    return 0;
}












