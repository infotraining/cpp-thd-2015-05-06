#include <iostream>
#include <thread>
#include <string>
#include <vector>
#include <atomic>
#include <mutex>

using namespace std;

class spinlock
{
    atomic_flag flag;
public:
    spinlock() : flag(ATOMIC_FLAG_INIT)
    {}

    void lock()
    {
        while(flag.test_and_set());
    }

    void unlock()
    {
        flag.clear();
    }
};

class ScopedMutex
{
    mutex& mtx_;
public:
    ScopedMutex(mutex& mtx) : mtx_(mtx)
    {
        mtx_.lock();
    }
    ~ScopedMutex()
    {
        mtx_.unlock();
    }
    ScopedMutex(ScopedMutex&) = delete;
    ScopedMutex& operator=(ScopedMutex&) = delete;
};

volatile long counter{};
long counter_mtx{};
mutex mtx;
atomic<long> counter_atomic{};

void increase()
{
    for(int i = 0 ; i < 1000000 ; ++i)
        ++counter;
}

void increase_atomic()
{
    for(int i = 0 ; i < 1000000 ; ++i)
        counter_atomic.fetch_add(1);
}

spinlock sl;

void increase_mtx()
{
    for(int i = 0 ; i < 1000000 ; ++i)
    {
        lock_guard<spinlock> m(sl);
        ++counter_mtx;
        //if(counter_mtx == 1000) return;
    }
}

int main()
{
    vector<thread> thds;
    atomic<double> at_d;
    cout << "is lf double " << at_d.is_lock_free() << endl;
//    atomic<long double> at_ld;
//    cout << sizeof(long double) << endl;
//    cout << "is lf long double " << at_ld.is_lock_free() << endl;

    auto start = chrono::high_resolution_clock::now();

    for(int i = 0 ; i < 4 ; ++i)
        thds.emplace_back(increase);

    for(auto& th : thds) th.join();

    auto finish = chrono::high_resolution_clock::now();
    cout << "Elapsed ";
    cout << chrono::duration_cast<chrono::milliseconds>(finish-start).count();
    cout << " ms" << endl;
    cout << "Counter = " << counter << endl;
    //*******************************************
    thds.clear();

    start = chrono::high_resolution_clock::now();
    for(int i = 0 ; i < 4 ; ++i)
        thds.emplace_back(increase_atomic);

    for(auto& th : thds) th.join();

    finish = chrono::high_resolution_clock::now();
    cout << "Elapsed ";
    cout << chrono::duration_cast<chrono::milliseconds>(finish-start).count();
    cout << " ms" << endl;
    cout << "is this counter lock-free? " << counter_atomic.is_lock_free() << endl;
    cout << "Atomic Counter = " << counter_atomic.load() << endl;

    //*******************************************
    thds.clear();

    start = chrono::high_resolution_clock::now();
    for(int i = 0 ; i < 4 ; ++i)
        thds.emplace_back(increase_mtx);

    for(auto& th : thds) th.join();

    finish = chrono::high_resolution_clock::now();
    cout << "Elapsed ";
    cout << chrono::duration_cast<chrono::milliseconds>(finish-start).count();
    cout << " ms" << endl;
    cout << "Mutex Counter = " << counter_mtx << endl;
    return 0;
}

