#include <iostream>
#include <random>
#include <chrono>
#include <thread>
#include <atomic>

using namespace std;

void calc(atomic<long>& cnt, long N)
{
    random_device rd;
    mt19937_64 mt(rd());
    uniform_real_distribution<long double> dist(-1, 1);
    long local_cnt{};
    for(int i = 0 ; i < N ; ++i)
    {
        long double x = dist(mt);
        long double y = dist(mt);
        if( x*x + y*y < 1)
            ++local_cnt;
    }
    cnt += local_cnt;
}

int main()
{

    auto start = chrono::high_resolution_clock::now();

    atomic<long> cnt{};
    const long N = 1000000;
    cout << "n of available cores = " << thread::hardware_concurrency() << endl;
    int n_of_threads = 4;
    vector<long> counters(n_of_threads);
    vector<thread> threads;

    //for(auto& cnt : counters)
    for (int i = 0 ; i < n_of_threads ; ++i)
        threads.emplace_back(calc, ref(cnt), N/n_of_threads);

    for(auto& th : threads) th.join();
    //cnt = accumulate(counters.begin(), counters.end(), 0L);
    auto finish = chrono::high_resolution_clock::now();
    cout << "Elapsed ";
    cout << chrono::duration_cast<chrono::milliseconds>(finish-start).count();
    cout << " ms" << endl;

    cout << "Pi = " << double(cnt)/N * 4 << endl;

    return 0;
}

