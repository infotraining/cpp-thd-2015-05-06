#include <iostream>
#include <thread>
#include <string>
#include <vector>

using namespace std;

void hello()
{
    cout << "Hello world" << endl;
}

void say_id(int id, string msg)
{
    cout << msg << " " << id << endl;
}

void increase(vector<int>& v)
{
    for(auto& el : v)
        ++el;

    for(auto& el : v)
        cout << el <<",";
    cout << endl;
}

thread generator()
{
    vector<int> v{1,2,3,4};
    //return thread(increase, ref(v)); //bad
    return thread([v] () mutable { increase(v);});
}

int main()
{

    thread th2(say_id, 10, "hello");
    thread th1 = generator();
    cout << "is th1 joinable? " << th1.joinable() << endl;
    cout << "is th2 joinable? " << th2.joinable() << endl;

    th1.join();
    th2.join();

    vector<int> v{3,4,5};

    vector<thread> threads;
    threads.push_back(thread(hello));
    threads.push_back(generator());
    threads.emplace_back(hello);
    threads.emplace_back(increase, ref(v));

    for(auto& th : threads) th.join();
    return 0;
}

