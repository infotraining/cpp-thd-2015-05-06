#include <iostream>
#include <thread>
#include <string>
#include <vector>
#include <atomic>
#include <mutex>
#include <algorithm>
#include <functional>
#include "../../utils.h"
#include <future>

using namespace std;

struct Node
{
    Node* prev;
    int data;
};

class Stack
{
    atomic<Node*> head;
public:
    void push(int i)
    {
        Node* new_node = new Node;
        new_node->data = i;

        new_node->prev = head.load();
        while(!head.compare_exchange_weak(new_node->prev, new_node));
    }

    int pop()
    {
        Node* old_node = head.load();
        int res = old_node->data;
        while(!head.compare_exchange_weak(old_node, old_node->prev));
        delete old_node;
        old_node = nullptr;
        return res;
    }
};

void test_stack(Stack& s)
{
    for (int i = 0 ; i < 1000 ; ++i)
    {
        s.push(i);
    }
    for (int i = 0 ; i < 1000 ; ++i)
    {
        cout << s.pop() << ", ";
    }
    cout << endl;
}

int main()
{
    Stack s;
    scoped_thread st1(test_stack, ref(s));
    scoped_thread st2(test_stack, ref(s));
    scoped_thread st3(test_stack, ref(s));
    scoped_thread st4(test_stack, ref(s));
    scoped_thread st5(test_stack, ref(s));
    return 0;
}

