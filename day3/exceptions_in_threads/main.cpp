#include <iostream>
#include <thread>
#include <string>
#include <vector>
#include <atomic>
#include <mutex>
#include <algorithm>
#include <functional>
#include "../../utils.h"
#include <future>

using namespace std;

void can_throw(int input, exception_ptr& eptr)
{
    try
    {
        if(input == 13) throw std::logic_error("bad luck");
    }
    catch(...)
    {
        eptr = current_exception();
    }
}

int main()
{
    exception_ptr eptr;
    try
    {
        {
            scoped_thread(can_throw, 13, ref(eptr));
        }
        if(eptr)
            rethrow_exception(eptr);
    }
    catch(std::logic_error& err)
    {
        cerr << "Error: " << err.what() << endl;
    }

    return 0;
}

