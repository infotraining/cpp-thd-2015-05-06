#include <iostream>
#include <thread>
#include <string>
#include <vector>
#include <atomic>
#include <mutex>
#include <algorithm>
#include <functional>
#include "../../utils.h"
#include <future>

using namespace std;

template<typename It, typename T>
future<T> async_parallel_accumulate(It begin, It end, T init)
{
    size_t lenght = distance(begin, end);
    unsigned int n_of_workers = 4;
    decltype(lenght) segment_size = lenght/n_of_workers;
    vector<shared_future<T>> results;

    It start = begin;
    It finish = begin;
    for(unsigned int i = 0 ; i < n_of_workers ; ++i)
    {
        advance(finish, segment_size);
        if(i == n_of_workers-1) finish = end;
        results.push_back(async(launch::async,
                                &accumulate<It, T>,
                                start,
                                finish,
                                T()));
        start = finish;
    }



    //return async(launch::deferred, [results=move(results), init] () mutable {
    return async(launch::deferred, [results, init] () mutable {
        for(auto& fut : results)
        {
            init += fut.get();
        }
        return init;
    });

//    return accumulate(results.begin(), results.end(), init,
//                      +[] (const T& a, future<T>&b ) { return a + b.get();});
}

int main()
{
    vector<long> v;
    for(int i = 0 ; i < 40'000'000 ; ++i)
    {
        v.push_back(i);
    }
    // sync version
    auto start = chrono::high_resolution_clock::now();
    cout << std::accumulate(v.begin(), v.end(), 0L) << endl;
    auto finish = chrono::high_resolution_clock::now();
    cout << "Sync elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(finish-start).count();
    cout << " ms" << endl;

    // parallel version
    start = chrono::high_resolution_clock::now();
    future<long> res = async_parallel_accumulate(v.begin(), v.end(), 0L);
    cout << res.get()  << endl;
    finish = chrono::high_resolution_clock::now();
    cout << "parallel elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(finish-start).count();
    cout << " ms" << endl;
    return 0;
}

