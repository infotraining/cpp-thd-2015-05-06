#include <iostream>
#include <thread>
#include <string>
#include <vector>
#include <atomic>
#include <mutex>
#include <algorithm>
#include <functional>
#include "../../utils.h"
#include <future>

using namespace std;

class Service
{
public:
    virtual void run() = 0;
    virtual ~Service() {}
};

class RealService : public Service
{
    string url_;
public:
    RealService(string url) : url_(url)
    {
        cout << "Creating real service" << endl;
        this_thread::sleep_for(2s);
        cout << "Service is ready" << endl;
    }

    void run () override
    {
        cout << "RealService::run" << endl;

    }

    ~RealService()
    {

    }
};

namespace bad
{

class ProxyService : public Service
{
    RealService* real_service{nullptr};
    string url_;
    mutex mtx;
public:
    ProxyService(string url) : url_(url)
    {
    }

    void run() override
    {
        if(real_service == nullptr)
        {
            lock_guard<mutex> lg(mtx);
            {
                if(real_service == nullptr)
                real_service = new RealService(url_);
            }
        }
        real_service->run();
    }

    ~ProxyService()
    {
        delete real_service;
    }
};
}

namespace working_atom
{
class ProxyService : public Service
{
    atomic<RealService*> real_service{nullptr};
    string url_;
    mutex mtx;
public:
    ProxyService(string url) : url_(url)
    {
    }

    void run() override
    {
        if(real_service.load() == nullptr)
        {
            lock_guard<mutex> lg(mtx);
            {
                if(real_service.load() == nullptr)
                real_service.store(new RealService(url_));
            }
        }
        (*real_service).run();
    }

    ~ProxyService()
    {
        delete real_service;
    }
};
}

namespace working_once
{
class ProxyService : public Service
{
    RealService* real_service{nullptr};
    string url_;
    once_flag flag;
public:
    ProxyService(string url) : url_(url)
    {
    }

    void run() override
    {
        call_once(flag, [this] { real_service = new RealService(url_);});
        real_service->run();
    }

    ~ProxyService()
    {
        delete real_service;
    }
};
}

using namespace working_once;

int main()
{
    ProxyService service("ala");
    scoped_thread th1(&ProxyService::run, &service);
    scoped_thread th2(&ProxyService::run, &service);
    return 0;
}

