#include <iostream>
#include <thread>
#include <string>
#include <vector>
#include <atomic>
#include <mutex>
#include <algorithm>
#include <functional>
#include "../../utils.h"
#include <future>

using namespace std;

int question()
{
    this_thread::sleep_for(50ms);
    return 42;
}

void can_throw(int input)
{
    if(input == 13) throw std::logic_error("bad luck");
}

int main()
{
    int answer;
    thread th1([&answer] { answer = question();});
    th1.join();
    cout << "Result = " << answer << endl;

    future<int> ans = async(launch::async, question);
    cout << "Result via future " << ans.get() << endl;

    vector<future<int>> results;
    for(int i = 0 ; i < 10 ; ++i)
    {
        results.push_back(async(launch::async, question));
    }
    for (auto& fut : results)
    {
        cout << "got " << fut.get() << endl;
    }

    cout << "throwing function" << endl;
    future<void> res = async(launch::async, can_throw, 13);
    res.wait();
    try
    {
//        thread th1(can_throw, 13);
//        th1.join();
        res.get();
    }
    catch(std::logic_error& err)
    {
        cerr << "Error: " << err.what() << endl;
    }
    return 0;
}

