#include <iostream>
#include <thread>
#include <string>
#include <vector>
#include <atomic>
#include <mutex>
#include <algorithm>
#include <functional>
#include "../../utils.h"
#include <future>

using namespace std;

int question()
{
    this_thread::sleep_for(500ms);
    throw std::bad_alloc();
    return 42;
}

void can_throw(int input)
{
    if(input == 13) throw std::logic_error("bad luck");
}

class my_packaged_task
{
    function<int()> f_;
    promise<int> p;
public:
    my_packaged_task(function<int()> f) : f_(f)
    {}
    void operator()()
    {
        try{
            p.set_value(f_());
        }
        catch(...)
        {
            p.set_exception(current_exception());
        }
    }

    future<int> get_future()
    {
        return p.get_future();
    }

};

int main()
{
    //packaged_task<int()> pt(question);
    my_packaged_task pt(question);
    future<int> res = pt.get_future(); // void(*pt)()
    thread t(move(pt));
    try {
        cout << "result = " << res.get() << endl;
    }
    catch(...)
    {
        cerr << "Some error" << endl;
    }

    t.join();
    return 0;
}

