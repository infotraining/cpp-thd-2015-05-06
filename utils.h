#ifndef UTILS_H
#define UTILS_H

#include <thread>
#include <mutex>
#include <queue>
#include <vector>
#include <condition_variable>
#include <functional>
#include <future>

class scoped_thread
{
    std::thread thd;
public:
    template<typename... Args>
    scoped_thread(Args&&... args) : thd(std::forward<Args>(args)...)
    {

    }

    scoped_thread(const scoped_thread&) = delete;
    scoped_thread& operator=(const scoped_thread&) = delete;

    scoped_thread(scoped_thread&&) = default;
    scoped_thread& operator=(scoped_thread&&) = default;

    ~scoped_thread()
    {
        if(thd.joinable()) thd.join();
    }
};

template<typename T>
class thread_safe_queue
{
    std::queue<T> q;
    std::mutex mtx;
    std::condition_variable cond_pop;
    std::condition_variable cond_push;
    size_t limit{0};
public:
    thread_safe_queue(size_t limit) : limit(limit) {}

    void push(T item)
    {
        std::unique_lock<std::mutex> ul(mtx);
        while(q.size() >= limit)
            cond_push.wait(ul);
        q.push(item);
        cond_pop.notify_one();
    }

    T pop()
    {
        std::unique_lock<std::mutex> ul(mtx);
        while(q.empty())
            cond_pop.wait(ul);
        T res = q.front();
        q.pop();
        cond_push.notify_one();
        return res;
    }
};

using task_t = std::function<void()>;

class thread_pool
{
    thread_safe_queue<task_t> q{10000};
    std::vector<scoped_thread> workers;

public:
    thread_pool(int size)
    {
        for(int i = 0 ; i < size ; ++i)
            workers.emplace_back([this]
            {
                for(;;)
                {
                    auto task = q.pop();
                    if(!task) return;
                    task();
                }
            });
    }

    void submit(task_t task)
    {
        if (task)
            q.push(task);
    }

    template<typename F>
    auto async(F f) -> std::future<decltype(f())>
    {
        using res_t = decltype(f());
        auto pt = std::make_shared<std::packaged_task<res_t()>>(f);
        std::future<res_t> res = pt->get_future();
        q.push([pt] { (*pt)(); });
        return res;
    }

    ~thread_pool()
    {
        //for(auto& th : workers) th.join();
        for(unsigned int i = 0 ; i < workers.size() ; ++i)
            q.push(task_t());
    }


};

#endif // UTILS_H

