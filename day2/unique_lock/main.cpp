#include <iostream>
#include <thread>
#include <string>
#include <vector>
#include <atomic>
#include <mutex>
#include "../../utils.h"

using namespace std;
timed_mutex mtx;

void worker_old(int id)
{
    cout << "Worker " << id << " started" << endl;
    for(;;)
    {
        if(mtx.try_lock())
        {
            cout << "I'v got mutex " << id << endl;
            cout << this_thread::get_id() << endl;
            //this_thread::sleep_for(chrono::seconds(2));
            this_thread::sleep_for(2s);
            mtx.unlock();
            return;
        }
        else
        {
            cout << id << " is waiting for mutex " << endl;
            this_thread::sleep_for(500ms);
        }
    }
}

void worker(int id)
{
    cout << "Worker " << id << " started" << endl;
    unique_lock<timed_mutex> ul(mtx, try_to_lock);
    if(!ul.owns_lock())
    {
        do
        {
            cout << id << " is waiting for mutex" << endl;
        }
        while(!ul.try_lock_for(500ms));
    }
    cout << "I'v got mutex " << id << endl;
    this_thread::sleep_for(2s);
    return;
}


int main()
{
    vector<scoped_thread> thds;
    //scoped_thread a(worker, 1);
    //scoped_thread b(worker, 2);
    thds.emplace_back(worker, 1);
    thds.emplace_back(worker, 2);
    thds.emplace_back(worker, 3);
    return 0;
}

