#include <iostream>
#include <thread>
#include <string>
#include <vector>
#include <atomic>
#include <mutex>
#include <algorithm>
#include <functional>
#include <fstream>
#include "../../utils.h"

using namespace std;

class Log
{
    ofstream f;
    //mutex mtx;
    thread_pool tp{1};
public:
    Log(string fname)
    {
        f.open(fname);
        if(!f)
            cerr << "error opening file" << endl;
    }

    void log(string msg)
    {
        tp.submit([this, msg] {
        //lock_guard<mutex> lg(mtx);
            f << msg << endl;
        });
    }
};

void test_log(Log& log, string msg)
{
    for(int i = 0 ; i < 10000 ; ++i)
        log.log(msg + to_string(i));
}

int main()
{
    auto start = chrono::high_resolution_clock::now();
    {
    Log log("test.txt");
        scoped_thread sc1(test_log, ref(log), "first message ");
        scoped_thread sc2(test_log, ref(log), "second message ");
    }
    auto end = chrono::high_resolution_clock::now();
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us" << endl;

    return 0;
}

