#include <iostream>
#include <thread>
#include <string>
#include <vector>
#include <atomic>
#include <mutex>
#include <algorithm>
#include "../../utils.h"
#include <queue>
#include <condition_variable>

using namespace std;

queue<int> q;
mutex mtx;
condition_variable cond;

void producer()
{
    for(int i = 1 ; i < 100 ; ++i)
    {
        this_thread::sleep_for(500ms);
        {
            lock_guard<mutex> lg(mtx);
            q.push(i);
            cond.notify_one();
            cout << "just produced " << i << endl;
        }
    }
}

void consumer(int id)
{
    for(;;)
    {
        unique_lock<mutex> ul(mtx);
        while(q.empty())
        {
            cond.wait(ul);
        }
        //cond.wait(ul, [] { return !q.empty();});
        int msg = q.front();
        q.pop();
        cout << id << " just got " << msg << endl;
    }
}

int main()
{
    vector<scoped_thread> thds;
    thds.emplace_back(producer);
    thds.emplace_back(consumer, 1);
    thds.emplace_back(consumer, 2);
    return 0;
}

