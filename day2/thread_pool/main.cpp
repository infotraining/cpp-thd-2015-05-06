#include <iostream>
#include <thread>
#include <string>
#include <vector>
#include <atomic>
#include <mutex>
#include <algorithm>
#include <functional>
#include "../../utils.h"

using namespace std;

void process(function<void()> f)
{
    f();
}

void hello()
{
    cout << "Hi!!" << endl;
}

int q()
{
    return 42;
}

int main()
{
    cout << "Hello World!" << endl;
    process(hello);
//    auto ptr = make_unique<int>(10);
//    auto f = [ptr = move(ptr)] { cout << "lambda " << *ptr << endl;};
//    process(f);
    process( [] { cout << "Lambda" << endl;});
    function<void()> f;
    f = hello;
    if (f)
        f();

    thread_pool tp(4);
    tp.submit(hello);
    tp.submit([] { cout << "lambda task" << endl;});

    for(int i = 0 ; i < 10 ; ++i)
    {
        tp.submit([i] {
            cout << "Lambda " << i << endl;
            this_thread::sleep_for(500ms);
        });
    }

    vector<future<int>> res;
    for (int i = 0 ; i < 1000 ; ++i)
        res.push_back(tp.async(q));

    for (auto& fut : res)
        cout << fut.get();
    cout << endl;
    return 0;
}

