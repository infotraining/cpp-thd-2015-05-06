#include <iostream>
#include <thread>
#include <string>
#include <vector>
#include <atomic>
#include <mutex>
#include <algorithm>
#include "../../utils.h"

using namespace std;

class Data
{
    vector<int> data_;
    atomic<bool> is_ready{false};
public:
    void read()
    {
        data_.resize(20);
        cout << "reading data" << endl;
        generate(data_.begin(), data_.end(), [] {return rand() % 20;});
        this_thread::sleep_for(20s);
        is_ready = true;
    }

    void process()
    {
        for(;;)
        {
            if(is_ready)
            {
                cout << "processing" << endl;
                cout << "sum = " << accumulate(data_.begin(), data_.end(), 0) << endl;
                return;
            }
            //this_thread::yield();
            this_thread::sleep_for(10us);
        }
    }
};

int main()
{
    Data data;
    scoped_thread st1(&Data::read, &data);
    scoped_thread st2(&Data::process, &data);
    scoped_thread st3(&Data::process, &data);
    return 0;
}

