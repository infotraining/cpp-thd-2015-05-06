#include <iostream>
#include <thread>
#include <string>
#include <vector>
#include <atomic>
#include <mutex>
#include <algorithm>
#include "../../utils.h"
#include <queue>
#include <condition_variable>

using namespace std;

thread_safe_queue<int> q(64);

void producer()
{
    for(int i = 1 ; i < 1000000000 ; ++i)
    {
        q.push(i);
        cout << "just produced " << i << endl;
    }
}

void consumer(int id)
{
    for(;;)
    {
        int msg = q.pop();
        cout << id << " just got " << msg << endl;
        this_thread::sleep_for(500ms);
    }
}

int main()
{
    vector<scoped_thread> thds;

    thds.emplace_back(consumer, 1);
    thds.emplace_back(consumer, 2);
    producer();
    return 0;
}

